module Week5 where

import Std

type String = Star ⊸ Star
type Tup = (String ⊸ String ⊸ String) ⊸ String

hij_zal :: Tup ⊸ String
hij_zal = \q -> q (\x y -> Hij . Zal . x . y)

hij_zal_x :: Tup ⊸ Tup ⊸ String
hij_zal_x = \p q -> p (\x y -> q (\z w -> Hij . Zal . x . y . z . w))

haar :: String
haar = Haar

iets :: String
iets = Iets

zeggen :: String ⊸ Tup
zeggen = \a f -> f a Zeggen

laten :: Tup ⊸ String ⊸ Tup
laten = \q x f -> q (\z w -> f (x . z) (Laten . w))

te :: Tup ⊸ Tup
te = \q f -> q (\x y -> f x (Te . y))

om :: Tup ⊸ Tup
om = \q f -> q (\x y -> f x (y . Om))

proberen :: Tup ⊸ Tup
proberen = \q f -> q (\x y -> f x (Proberen . y))

proberen_x :: Tup
proberen_x = \f -> f (\a -> a) Proberen

willen :: Tup ⊸ Tup
willen = \q f -> q (\x y -> f x (Willen . y))

vertrekken :: Tup
vertrekken = \f -> f (\a -> a) Vertrekken

sample0 :: String
sample0 =
  hij_zal
    (willen
      (proberen
        (te
          (laten
            (zeggen iets)
            haar
          )
        )
      )
    )

-- >>> sample0 FormalParameter
-- Hij (Zal (Haar (Iets (Willen (Proberen (Te (Laten (Zeggen FormalParameter))))))))

sample1 :: String
sample1 =
  hij_zal_x
    (willen proberen_x)
    (te
      (laten
        (zeggen iets)
        haar
      )
    )

-- >>> sample1 FormalParameter
-- Hij (Zal (Willen (Proberen (Haar (Iets (Te (Laten (Zeggen FormalParameter))))))))

sample2 :: String
sample2 =
  hij_zal_x
    (willen (om proberen_x))
    (te
      (laten
        (zeggen iets)
        haar
      )
    )

-- >>> sample2 FormalParameter
-- Hij (Zal (Willen (Proberen (Om (Haar (Iets (Te (Laten (Zeggen FormalParameter)))))))))

data Star where
  Hij :: Star ⊸ Star
  Zal :: Star ⊸ Star
  Haar :: Star ⊸ Star
  Iets :: Star ⊸ Star
  Zeggen :: Star ⊸ Star
  Laten :: Star ⊸ Star
  Te :: Star ⊸ Star
  Om :: Star ⊸ Star
  Proberen :: Star ⊸ Star
  Willen :: Star ⊸ Star
  Vertrekken :: Star ⊸ Star
  FormalParameter :: Star
  deriving Show

(.) :: (b ⊸ c) ⊸ (a ⊸ b) ⊸ (a ⊸ c)
f . g = \x -> f (g x)
