module Composition where

import Std

data A = A
data B = B
data C = C

x :: A ⊸ B
x = undefined

y :: B ⊸ C
y = undefined

composition :: A ⊸ C
composition = \z -> y (x z)
