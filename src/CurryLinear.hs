module CurryLinear where

import Std

data A = A
data C = C

x :: (A, A) ⊸ C
x = undefined

curried :: A ⊸ A ⊸ C
curried = \y -> \z -> x (y, z)
