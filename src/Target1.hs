module Target1 where

import Std

-- begin of source logic

data S where
  Production0 :: A ⊸ S
  deriving Show

data A where
  Production1 :: NP ⊸ ITV ⊸ A
  Production2 :: NP ⊸ TV ⊸ A ⊸ A
  deriving Show

data NP where
  ProductionTomas :: NP
  ProductionYanmin :: NP
  ProductionSilviu :: NP
  deriving Show

data ITV where
  ProductionDansen :: ITV
  ProductionFietsen :: ITV
  deriving Show

data TV where
  ProductionZien :: TV
  ProductionLeren :: TV
  deriving Show

-- begin of target logic

data Star where
  Iemand :: Star ⊸ Star
  Hoort :: Star ⊸ Star
  Tomas :: Star ⊸ Star
  Yanmin :: Star ⊸ Star
  Silviu :: Star ⊸ Star
  Dansen :: Star ⊸ Star
  Fietsen :: Star ⊸ Star
  Zien :: Star ⊸ Star
  Leren :: Star ⊸ Star
  FormalParameter :: Star
  deriving Show

data List where
  Nil :: List
  Cons :: Tup ⊸ List ⊸ List
  deriving Show

-- begin of interpretation

type String = Star ⊸ Star
type Tup = (String ⊸ String ⊸ Text) ⊸ Text

{-
  \eta(S) == List
  \eta(A) == List
  \eta(NP) == String
  \eta(ITV) == String
  \eta(TV) == String

  \theta(ProductionX) == productionX
-}

production0 :: List ⊸ List
production0 = \q -> q

production1 :: String ⊸ String ⊸ List
production1 = \x y -> Cons (\f -> f x y) Nil

production2 :: String ⊸ String ⊸ List ⊸ List
production2 = \x y q -> Cons (\f -> f x y) q

productionTomas :: String
productionTomas = Tomas

productionYanmin :: String
productionYanmin = Yanmin

productionDansen :: String
productionDansen = Dansen

productionFietsen :: String
productionFietsen = Fietsen

productionZien :: String
productionZien = Zien

productionLeren :: String
productionLeren = Leren

-- begin of usage

interpretS :: S ⊸ List
interpretS (Production0 a) = production0 (interpretA a)

interpretA :: A ⊸ List
interpretA (Production1 np itv) =
  production1 (interpretNP np) (interpretITV itv)
interpretA (Production2 np tv a) =
  production2 (interpretNP np) (interpretTV tv) (interpretA a)

interpretNP :: NP ⊸ String
interpretNP ProductionTomas = Tomas
interpretNP ProductionYanmin = Yanmin
interpretNP ProductionSilviu = Silviu

interpretITV :: ITV ⊸ String
interpretITV ProductionDansen = Dansen
interpretITV ProductionFietsen = Fietsen

interpretTV :: TV ⊸ String
interpretTV ProductionZien = Zien
interpretTV ProductionLeren = Leren

sample :: S
sample =
  Production0
    (Production2
      ProductionTomas
      ProductionZien
      (Production2
        ProductionYanmin
        ProductionLeren
        (Production1
          ProductionSilviu
          ProductionFietsen
        )
      )
    )

-- >>> sample

interpretation :: List
interpretation = interpretS sample

-- >>> interpretation
-- Cons (Tomas FormalParameter, Zien FormalParameter) (Cons (Yanmin FormalParameter, Leren FormalParameter) (Cons (Silviu FormalParameter, Fietsen FormalParameter) Nil))

-- helpers

instance Show Tup where
  show q =
    q 
      (toLinearUnsafe
        (\x y -> 
          "(" <> show (x FormalParameter) <> ", " <>
          show (y FormalParameter) <> ")"
        )
      )
