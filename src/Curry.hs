module Curry where

import Std

data A = A
data B = B
data C = C

x :: (A, B) -> C
x = undefined

curried :: A -> B -> C
curried = \y -> \z -> x (y, z)
