module Std (module Std, module Prelude) where

import Unsafe.Coerce (unsafeCoerce)
import Prelude (Show (show), (<>), undefined)
import qualified Prelude

type Text = Prelude.String

toLinearUnsafe :: (a -> b -> c) ⊸ (a ⊸ b ⊸ c)
toLinearUnsafe = unsafeCoerce unsafeCoerce
