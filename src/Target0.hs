module Target0 where

import Std

-- begin of source logic

data S where
  Production0 :: A ⊸ S
  deriving Show

data A where
  Production1 :: NP ⊸ ITV ⊸ A
  Production2 :: NP ⊸ TV ⊸ A ⊸ A
  deriving Show

data NP where
  ProductionTomas :: NP
  ProductionYanmin :: NP
  ProductionSilviu :: NP
  deriving Show

data ITV where
  ProductionDansen :: ITV
  ProductionFietsen :: ITV
  deriving Show

data TV where
  ProductionZien :: TV
  ProductionLeren :: TV
  deriving Show

-- begin of target logic

data Star where
  Iemand :: Star ⊸ Star
  Hoort :: Star ⊸ Star
  Tomas :: Star ⊸ Star
  Yanmin :: Star ⊸ Star
  Silviu :: Star ⊸ Star
  Dansen :: Star ⊸ Star
  Fietsen :: Star ⊸ Star
  Zien :: Star ⊸ Star
  Leren :: Star ⊸ Star
  FormalParameter :: Star
  deriving Show

-- begin of interpretation

type String = Star ⊸ Star
type Tup = (String ⊸ String ⊸ String) ⊸ String

{-
  \eta(S) == String
  \eta(A) == Tup
  \eta(NP) == String
  \eta(ITV) == String
  \eta(TV) == String

  \theta(ProductionX) == productionX
-}

(+) :: (b ⊸ c) ⊸ (a ⊸ b) ⊸ (a ⊸ c)
f + g = \x -> f (g x)

production0 :: Tup ⊸ String
production0 = \q -> q (\x y -> Iemand + Hoort + x + y)

production1 :: String ⊸ String ⊸ Tup
production1 = \x y -> \f -> f x y

production2 :: String ⊸ String ⊸ Tup ⊸ Tup
production2 = \x y q -> \f -> q (\z u -> f (x + z) (y + u))

productionTomas :: String
productionTomas = Tomas

productionYanmin :: String
productionYanmin = Yanmin

productionDansen :: String
productionDansen = Dansen

productionFietsen :: String
productionFietsen = Fietsen

productionZien :: String
productionZien = Zien

productionLeren :: String
productionLeren = Leren

-- begin of usage

interpretS :: S ⊸ String
interpretS (Production0 a) = production0 (interpretA a)

interpretA :: A ⊸ Tup
interpretA (Production1 np itv) =
  production1 (interpretNP np) (interpretITV itv)
interpretA (Production2 np tv a) =
  production2 (interpretNP np) (interpretTV tv) (interpretA a)

interpretNP :: NP ⊸ String
interpretNP ProductionTomas = Tomas
interpretNP ProductionYanmin = Yanmin
interpretNP ProductionSilviu = Silviu

interpretITV :: ITV ⊸ String
interpretITV ProductionDansen = Dansen
interpretITV ProductionFietsen = Fietsen

interpretTV :: TV ⊸ String
interpretTV ProductionZien = Zien
interpretTV ProductionLeren = Leren

sample :: S
sample =
  Production0
    (Production2
      ProductionTomas
      ProductionZien
      (Production2
        ProductionYanmin
        ProductionLeren
        (Production1
          ProductionSilviu
          ProductionFietsen
        )
      )
    )

-- >>> sample

interpretation :: String
interpretation = interpretS sample

-- >>> interpretation FormalParameter
-- Iemand (Hoort (Tomas (Yanmin (Silviu (Zien (Leren (Fietsen FormalParameter)))))))

-- additional remark

c :: Tup ⊸ Tup
c = \q -> \f -> q (\x y -> f x y)

-- The previous works but the following does not.

-- c = \q -> q (\x y -> \f -> f x y)

-- But it would work with the following alternative `Tup` definition.

-- type Tup = forall r. (String ⊸ String ⊸ r) ⊸ r
