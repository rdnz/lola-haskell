module Uncurry where

import Std

data A = A
data C = C

x :: A ⊸ A ⊸ C
x = undefined

-- uncurried :: (A, A) ⊸ C
-- uncurried = \t -> case t of (a0, a1) -> x a0 a1
