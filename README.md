# lola-haskell

I recommend running the code as follows.

1. Install [Stack](https://docs.haskellstack.org/).
2. Install Visual Studio Code.
3. Install [the Haskell extension](https://marketplace.visualstudio.com/items?itemName=haskell.haskell).
4. Open the repository root directory in VS Code. The extension should offer to evaluate comments like [this example](https://gitlab.com/rdnz/lola-haskell/-/blob/b8c2029cd7941d8e93d064b3a14887e9555b2950/src/Week5.hs#L58). The extension might be slow to start up.
5. If the VS Code extension is too unreliable, [ghcid's evaluation feature](https://github.com/ndmitchell/ghcid#evaluation) is a good alternative.
